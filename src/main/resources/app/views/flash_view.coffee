define ->
  
  class FlashView extends Backbone.View
    tagName: "div"
    
    className: "alert"
    
    template: _.template $('#flash-template').html()
    
    initialize: (options) ->
      @render()
    
    render: ->
      @$el.addClass @options.flash.class if @options.flash.class
      @$el.html(@template(@options.flash)).prependTo('.flash')
